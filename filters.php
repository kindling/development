<?php
/**
 * Kindling Development - Filters.
 *
 * @package Kindling_Development
 */

if (!function_exists('add_action')) {
    return;
}

 add_action('kindling_ready', function () {
     /**
      * Enables Jetpack offline mode.
      *
      * @param boolean $enabled If Jetpack is currently enabled.
      */
     add_filter('jetpack_offline_mode', function ($enabled) {
         if (kindling_development_is_localhost()) {
             return true;
         }

         if (kindling_development_is_staging()) {
             return true;
         }

         return $enabled;
     });

     /**
      * Adds view directory to kindling-blade
      */
     add_filter('kindling_blade_view_paths', function ($paths) {
         $paths[] = KINDLING_DEVELOPMENT_DIR_PATH . '/views';

         return $paths;
     });
 });
