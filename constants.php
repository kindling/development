<?php
/**
 * Kindling Development - Constants.
 *
 * @package Kindling_Development
 */

if (!defined('ABSPATH')) {
    return;
}

define('KINDLING_DEVELOPMENT_DIR_PATH', rtrim(__DIR__, '/'));
define('KINDLING_DEVELOPMENT_URL', home_url(str_replace(ABSPATH, '', __DIR__)));
