<?php
/**
 * Kindling Development - Helpers.
 *
 * @package Kindling_Development
 */

/**
 * Checks the current host against the supplied hosts.
 *
 * @param   array $hosts  The possible hosts to check against.
 *
 * @return  boolean       True if the current host matches any of the supplied hosts, false if not.
 */
function kindling_development_check_hosts($hosts = [])
{
    $current = sanitize_text_field($_SERVER['HTTP_HOST']);

    // Check the possible HTTP hosts against the current HTTP host.
    if (in_array($current, $hosts)) {
        return true;
    }

    // Check the possible HTTP hosts against the current HTTP host.
    foreach ($hosts as $host) {
        if (false !== strpos($current, $host)) {
            return true;
        }
    } // foreach

    return false;
}

/**
 * Checks if the current HTTP host is localhost.
 * Default possible HTTP hosts http://localhost, 127.0.0.1, 10.0.0.2, http://*.dev.
 *
 * <code>
 * if (kindling_development_is_localhost()) {
 *  // Do something localhost specific...
 * }
 * </code
 *
 * @return  boolean If the current HTTP host is localhost.
 */
function kindling_development_is_localhost()
{
    // Default possible HTTP hosts URLs/IP addresses.
    $hosts = [
        'localhost',
        '127.0.0.1',
        '10.0.2.2',
        '.dev',
        '.test',
    ];

    /**
    * Allows for filtering of the possible HTTP hosts.
    * Accepts name/IP like localhost/127.0.0.1 or a domain such as .dev.
    *
    * @param  hosts  The possible HTTP hosts to check against.
    */
    $hosts = apply_filters('kindling_development_is_localhost_http_hosts', $hosts);

    return kindling_development_check_hosts($hosts);
}

/**
 * Checks if the current host is a staging site.
 *
 * @return  boolean If the current host is a staging site.
 */
function kindling_development_is_staging()
{
    $hosts = [
        'staging.',
        'dev.',
        '.flywheelsites.com',
        '.wpengine.com',
    ];

    /**
    * Allows for filtering of the possible HTTP hosts.
    * Accepts name/IP like localhost/127.0.0.1 or a domain such as .dev.
    *
    * @param  hosts  The possible HTTP hosts to check against.
    */
    $hosts = apply_filters('kindling_development_is_staging_http_hosts', $hosts);

    return kindling_development_check_hosts($hosts);
}

/**
 * Gets the environment type.
 *
 * @return string The environment type.
 */
function kindling_development_get_environment_type()
{
    if (kindling_development_is_localhost()) {
        $environment = 'local';
    } elseif (kindling_development_is_staging()) {
        $environment = 'staging';
    } else {
        $environment = 'production';
    }

    return apply_filters('kindling_development_get_environment_type', $environment);
}

/**
 * Checks if the current host is a production site.
 *
 * @return  boolean  If the current host is a production site.
 */
function kindling_development_is_production()
{
    return ('production' === kindling_development_get_environment_type());
}

if (!function_exists('pp')) {
    /**
     * Pretty Print Debug Function
     *
     * <code>
     * pp($value1, $value2,...);
     * </code>
     *
     * @param mixed ...$values Any value.
     */
    function pp(...$values)
    {
        if (kindling_development_is_production()) {
            return;
        }

        foreach ($values as $value) {
            echo '<pre>';
            if (is_string($value) or is_bool($value) or is_null($value)) {
                var_dump($value);
            } else {
                print_r($value);
            }
            echo '</pre>';
        }
    }
}

if (!function_exists('dump')) {
    /**
     * Pretty Print Debug Function
     *
     * <code>
     * dump($value1, $value2,...);
     * </code>
     *
     * @param mixed ...$values Any value.
     */
    function dump(...$values)
    {
        pp($values);
    }
}

if (! function_exists('dd')) {
    /**
     * Pretty Print Debug and Die
     *
     * <code>
     * dd($value1, $value2,...);
     * </code>
     *
     * @param mixed ...$values Any value.
     */
    function dd(...$values)
    {
        pp($values);

        die();
    }
}

/**
 * If the environment bar should be displayed.
 *
 * @return boolean True if the bar should be displayed and false if not
 */
function kindling_development_display_environment_bar()
{
    /**
    * Allows for filtering if the environment bar should be displayed or not.
    *
    * @param  boolean
    */
    return apply_filters('kindling_development_display_environment_bar', !kindling_development_is_production());
}

/**
 * Adds the environment bar.
 */
function kindling_development_environment_bar()
{
    if (kindling_development_display_environment_bar()) {
        include KINDLING_DEVELOPMENT_DIR_PATH  . '/views/environment-bar.php';
    }
}

/**
 * Adds the environment bar styles.
 */
function kindling_development_environment_bar_styles()
{
    if (kindling_development_display_environment_bar()) {
        $path = '/assets/environment-bar.css';
        wp_enqueue_style(
            'kindling-development/css',
            KINDLING_DEVELOPMENT_URL . $path,
            false,
            sha1_file(KINDLING_DEVELOPMENT_DIR_PATH . $path)
        );
    }
}

/**
 * Adds the environment bar body class.
 *
 * @param  array|string $classes The current classes.
 *
 * @return array|string          An array of classes if passed in as an array and a string if passed in as a string.
 */
function kindling_development_environment_bar_body_class($classes)
{
    if (! kindling_development_display_environment_bar()) {
        return $classes;
    }

    $env_type = kindling_development_get_environment_type();
    $bar_classes = apply_filters('kindling_development_environment_bar_body_class', [
        'kindling-development-environment-bar-enabled',
        "kindling-development-environment-{$env_type}",
    ]);

    if (is_string($classes)) {
        $classes .= trim(' ' . implode(' ', $bar_classes));
    } elseif (is_array($classes)) {
        $classes = array_merge($classes, $bar_classes);
    }

    return $classes;
}
