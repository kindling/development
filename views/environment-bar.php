<?php
$message = ucwords(kindling_development_get_environment_type());
$wordsLimit = 360 / strlen($message);
?>
<div class="kindling-development-environment-bar">
    <?php for ($i = 0; $i < $wordsLimit; $i++) {
    ?>
    <div class="kindling-development-environment-bar-message">
        <?php echo wp_kses_post($message); ?>
    </div>
    <?php
} ?>
</div>
