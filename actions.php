<?php
/**
 * Kindling Development - Actions.
 *
 * @package Kindling_Development
 */

if (!function_exists('add_action')) {
    return;
}

/**
 * Add the environment bar.
 */
add_action('kindling_ready', function () {
    add_action('admin_footer', 'kindling_development_environment_bar');
    add_action('wp_footer', 'kindling_development_environment_bar');
    add_action('admin_body_class', 'kindling_development_environment_bar_body_class');
    add_action('body_class', 'kindling_development_environment_bar_body_class');
    add_action('wp_enqueue_scripts', 'kindling_development_environment_bar_styles');
    add_action('admin_enqueue_scripts', 'kindling_development_environment_bar_styles');
});
